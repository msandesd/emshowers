#pragma link C++ class vector<Float_t>+;

Double_t FindMin(vector<Double_t> myVector){     
    // Initialize minimum element 
    Double_t min = myVector[0]; 
    
    // Traverse vector elements  
    for (unsigned int i = 1; i < myVector.size(); i++) {
        if (myVector[i] < min) 
            min = myVector[i]; 
    }
    return min;
}
