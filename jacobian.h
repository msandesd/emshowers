#include "FrameRef.h"
#include "TMath.h"
#ifndef JACOBIAN_H
#define JACOBIAN_H
double_t jacobian(Double_t R1, Double_t e1,Double_t f1, Double_t Rm, Double_t em, Double_t fm){
	double_t t1;
	t1= theta_to_Eta(e1);

    //if(e1==0)
    /*{*/return TMath::Abs((4*sqrt(pow(Rm,2)*pow(sech(em),2)*(pow(sin(f1 - fm),2) +pow(sinh(em),2))))/(-1 - 2*cos(2*(f1 - fm)) + 2*pow(cosh(em),2) +cosh(2*em)));/*}*/
    //else{return  TMath::Abs( (4*sin(t1)*sqrt(pow(Rm,2)*pow(sech(em),2)*pow(sin(t1),2)*(pow(cos(f1- fm),2)*pow(cotan(t1),2) + pow(sin(f1 - fm),2) - 2*cos(f1 -fm)*cotan(t1)*sinh(em) + pow(sinh(em),2))))/(2*pow(cosh(em),2) + cos(2*t1)*(1 + 2*cos(2*(f1 - fm))-cosh(2*em)) - 4*cos(f1 - fm)*sin(2*t1)*sinh(em)) );} //definition given by mathematica
}
#endif


