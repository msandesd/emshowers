#include "TMath.h"

#ifndef HEAVISIDE_H
#define HEAVISIDE_H

Double_t Heaviside(Double_t x){
  if (x<0){
    return 0. ;
  }
  else{
    return 1. ;        
  }
}

#endif
