#include "TMath.h"

#ifndef FRAMEREF_H
#define FRAMEREF_H
//Usefull functions defined by mathematica
Double_t cotan(Double_t x){
  return 1/tan(x);
  }
Double_t csc(Double_t x){
  return 1/sin(x);
  }
Double_t sech(Double_t x){
  return 1/cosh(x);
  }
Double_t acotan(Double_t x){
  return TMath::Pi()/2 -atan(x);
  }
  

//Frame of reference change


// R1,t1,f1 variables is where the E-/photon enters the calorimeter, Rm,tm,fm are for the cells

Double_t rcyl(Double_t R1, Double_t e1, Double_t f1,Double_t Rm, Double_t em, Double_t fm){
  Double_t t1;
  t1=2*atan(exp(-e1));
  Double_t r;
  r=sqrt(pow(Rm,2)*(pow(sech(em),2)*pow(sin(f1-fm),2)*pow(sin(t1),2) + pow(cos(f1 - fm)*cos(t1)*sech(em) -sin(t1)*atan(em),2)));
  return r;
}

//theta Projection of the cylindric frame of reference
Double_t alphacyl(Double_t R1, Double_t e1, Double_t f1,Double_t Rm, Double_t em, Double_t fm){
	Double_t t1;
	t1=2*atan(exp(-e1));
	Double_t r;
	r=atan(cotan(f1 - fm)*cotan(t1) - csc(f1 - fm)*sinh(em));
	if(r==r){
	  return r;
	}//check for nan, because of the non definition in r=0
	else{
	  return 0;
	}
}

//z Projection of the cylindric frame of reference

Double_t zcyl(Double_t R1, Double_t e1, Double_t f1,Double_t Rm, Double_t em, Double_t fm){
  Double_t t1;
  t1=2*atan(exp(-e1));
  return -R1 + Rm*cos(f1 - fm)*sech(em)*sin(t1) + Rm*cos(t1)*tanh(em);
}


double_t Eta_to_theta(double_t e){
  return 2*atan(exp(-e));
}
double_t theta_to_Eta(double_t theta){
  return -log(tan(theta/2));
}

#endif
