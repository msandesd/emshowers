    extern Double_t taud  =  15.82 ;
    extern Double_t taupa =  17.31 ;
    extern Double_t td    = 420.00 ;
    extern Double_t Rf    =   0.078 ;
    extern Double_t C1    =  50.00 ;
    extern Double_t Rin   =   1.20 ;
    extern Double_t Cx    =  47.00 ;
    extern Double_t Eth[89];   // Values of the theorectical Energy
    extern Double_t Tauth[89]; // Values of the theoretical Tau
